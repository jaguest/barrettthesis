/** This is a Cpp file Which synthesizes an input file to be read by
 * the cache simulator. By analyzing matrix multiply algorithms from both
 * the Python and Java perspectives, this file performs 'cache emissions'
 * which will build the input file. 
 * author Jason Guest
 * version 2.2020
 */

/* -----INCLUSIONS----- */
#include "CacheEmitter.hpp" /*For cache emission methods*/
#include "JavaSim.hpp"      /*For Java simulated code */
#include "PythonSim.hpp"    /*For Python simulated code */
#include <iostream>         /*Library for user input and console messages*/
#include <fstream>          /*Library for text file writing/reading methods*/
#include <string>           /*Library for use of string datatype, used in emitting cahe data to file*/
using namespace std;        
#define NUMROWS 1000
#define NUMCOLS 1000
/* -----METHODS----- */

/* Main entry and exit point for program execution */
int main()
{
    cout << "size of int: " << sizeof(int) << " bytes\n";
    cout << "size of double: " << sizeof(double) << " bytes\n";
    cout << "size of float: " << sizeof(float) << " bytes\n";
    cout << "Enter a filename in .txt format to create: " << "\n";
    char fileName[24];  /*Declare a variable to hold the input file name*/
    cin.get(fileName, 24);  /*Get the file name from user input*/
    ofstream outFile (fileName);    /*Create our file from the input to be written to*/

    if (outFile.is_open()) {    /*Perform actions if the file is open*/

        /*Our file is ready to be written to, now take input to see whether a Java or Python code shoud=ld be simulated*/
        cout << "Enter a 1 for Java code simulation, and a 0 for Python (1 is default): " << "\n";
        int choice = 1; /*Default the choice to 1*/
        cin >> choice;  /*Initialize the choice by input*/
        int whatType = 0;

        if (choice == 1) { /*default case is to look at Java*/
            cout << "Enter a 0 for integer, 1 for double, and 2 for float matrix multiply: \n";
            cin >> whatType;

            int i, j, k;
            if (whatType == 0) {
                /* Build integer matrices */
                int** javaIntOne = makeJavaArray(NUMCOLS, NUMROWS); /* ~16,000,000 bytes = 16 megabytes */
                int** javaIntTwo = makeJavaArray(NUMCOLS, NUMROWS);

                /* Below are the calulations to get the number of rows and columns for each matrix */
                /* These values will be read by the loops and multiply in the matrix multiply methods */
                int javaIntRowsOne = NUMROWS;
                int javaIntColsOne = NUMCOLS;
                int javaIntRowsTwo = NUMROWS;
                int javaIntColsTwo = NUMCOLS;

                /* integer matrix multiply */

                int** javaIntResult = makeJavaArray(NUMCOLS, NUMROWS);
                outFile << addressof(javaIntColsTwo) << "\n";
                outFile << addressof(javaIntRowsOne) << "\n";

                for (i = 0; i < javaIntRowsOne; i++) { /*Rows of first*/
                    /*CacheEmitter calls*/
                    /*reads*/
                    outFile << &i << "\n";
                    outFile << &javaIntRowsOne << "\n";
                    for (j = 0; j < javaIntColsTwo; j++) { /*Columns of second*/
                        /*CacheEmitter calls*/
                        /*reads*/
                        outFile << &j << "\n";
                        outFile << &javaIntColsTwo << "\n";
                        for (k = 0; k < javaIntColsOne; k++) { /*Columns of first*/
                            /*CacheEmitter calls*/
                            /*Reads and writes*/
                            outFile << &k << "\n";
                            outFile << &javaIntColsOne << "\n";
                            outFile << &javaIntOne[i][k] << "\n";
                            outFile << &javaIntTwo[k][j] << "\n";
                            javaIntResult[i][j] += javaIntOne[i][k] * javaIntTwo[k][j];
                            outFile << &javaIntResult[i][j] << "\n";
                            outFile << &k << "\n";
                        }
                        outFile << &j << "\n"; /* Address out to represent w write to increment counter value */
                    }
                    outFile << &i << "\n";
                }

                cout << &javaIntResult;

                /* Pointers will now be out of scope, so we need to delete */
                /* First delete all arrays within array, then the array of pointers */
                for (i = 0; i < NUMROWS; i++) {
                    delete(javaIntOne[i]);
                    delete(javaIntTwo[i]);
                }
                delete(javaIntOne);
                delete(javaIntTwo);

                if (outFile.is_open()) {
                    outFile.close();    /* Close file once finished writing */
                }
            }
            else if (whatType == 1) {   /* double */
                /* Build Double matrices */
                double** javaDoubleOne = makeJavaDoubleArray(NUMCOLS, NUMROWS); /* ~16,000,000 bytes = 16 megabytes */
                double** javaDoubleTwo = makeJavaDoubleArray(NUMCOLS, NUMROWS);

                /* Below are the calulations to get the number of rows and columns for each matrix */
                /* These values will be read by the loops and multiply in the matrix multiply methods */
                int javaDoubleRowsOne = NUMROWS;
                int javaDoubleColsOne = NUMCOLS;
                int javaDoubleRowsTwo = NUMROWS;
                int javaDoubleColsTwo = NUMCOLS;

                /*Double matrix multiply*/
                double** javaDoubleResult = makeJavaDoubleArray(javaDoubleColsTwo, javaDoubleRowsOne);
                outFile << &javaDoubleColsTwo << "\n";
                outFile << &javaDoubleRowsOne << "\n";

                for (i = 0; i < javaDoubleRowsOne; i++) { /*Rows of first*/
                    /*CacheEmitter calls*/
                    outFile << &i << "\n";
                    for (j = 0; j < javaDoubleColsTwo; j++) { /*Columns of second*/
                        /*CacheEmitter calls*/
                        outFile << &j << "\n";
                        for (k = 0; k < javaDoubleColsOne; k++) { /*Columns of first*/
                            /*CacheEmitter calls*/
                            outFile << &k << "\n";
                            outFile << &javaDoubleOne[i][k] << "\n";
                            outFile << &javaDoubleTwo[k][j] << "\n";
                            javaDoubleResult[i][j] += javaDoubleOne[i][k] * javaDoubleTwo[k][j];
                            outFile << &javaDoubleResult[i][j] << "\n";
                            outFile << &k << "\n";
                        }
                        outFile << &j << "\n";
                    }
                    outFile << &i << "\n";
                }

                /* Need to do something with the result[][] so that the loop isn't optimized away */
                cout << &javaDoubleResult;

                /* Pointers will now be out of scope, so we need to delete */
                /* First delete all arrays within array, then the array of pointers */
                for (i = 0; i < NUMROWS; i++) {
                    delete(javaDoubleOne[i]);
                    delete(javaDoubleTwo[i]);
                }
                delete(javaDoubleOne);
                delete(javaDoubleTwo);

                if (outFile.is_open()) {
                    outFile.close();    /* Close file once finished writing */
                }
            }
            else { // will default to float if invalid input
                /* Build float arrays */
                float** javaFloatOne = makeJavaFloatArray(NUMCOLS, NUMROWS); /* ~16,000,000 bytes = 16 megabytes */
                float** javaFloatTwo = makeJavaFloatArray(NUMCOLS, NUMROWS);

                int javaFloatRowsOne = NUMROWS;
                int javaFloatColsOne = NUMCOLS;
                int javaFloatRowsTwo = NUMROWS;
                int javaFloatColsTwo = NUMCOLS;

                /*Float matrix multiply*/
                float** javaFloatResult = makeJavaFloatArray(javaFloatColsTwo, javaFloatRowsOne);
                outFile << &javaFloatColsTwo << "\n";
                outFile << &javaFloatRowsOne << "\n";

                for (i = 0; i < javaFloatRowsOne; i++) { /*Rows of first*/
                    /*CacheEmitter calls*/
                    outFile << &i << "\n";
                    outFile << &javaFloatRowsOne << "\n";
                    for (j = 0; j < javaFloatColsTwo; j++) { /*Columns of second*/
                        /*CacheEmitter calls*/
                        outFile << &j << "\n";
                        outFile << &javaFloatColsTwo << "\n";
                        for (k = 0; k < javaFloatColsOne; k++) { /*Columns of first*/
                            /*CacheEmitter calls*/
                            outFile << &k << "\n";
                            outFile << &javaFloatColsOne << "\n";
                            outFile << &javaFloatOne[i][k] << "\n";
                            outFile << &javaFloatTwo[k][j] << "\n";
                            javaFloatResult[i][j] += javaFloatOne[i][k] * javaFloatTwo[k][j];
                            outFile << &javaFloatResult[i][j] << "\n";
                            outFile << &k << "\n";
                        }
                        outFile << &j << "\n";
                    }
                    outFile << &i << "\n";
                }

                cout << &javaFloatResult;

                /* Pointers will now be out of scope, so we need to delete */
                /* First delete all arrays within array, then the array of pointers */
                for (i = 0; i < NUMROWS; i++) {
                    delete(javaFloatOne[i]);
                    delete(javaFloatTwo[i]);
                }
                delete(javaFloatOne);
                delete(javaFloatTwo);

                if (outFile.is_open()) {
                    outFile.close();    /* Close file once finished writing */
                }
            }

               /* END JAVA MATRIX MULTIPLY SIMULATION */
        }
        else {  /*Should the user enter anything but a 1, we will look at Python code*/
            /* Rows first, columns second, rows second
                result[i][j] += first[i][k] * second[k][j] */

            cout << "Enter a 0 for integer, 1 for double, and 2 for float matrix multiply: \n";
            cin >> whatType;

            int i, j, k;
            int tempValue;

            if (whatType == 0) { /*Int*/
                /* Build the Python 2D arrays */
                int* pythonIntOne = makePythonArray(NUMCOLS, NUMROWS);
                int* pythonIntTwo = makePythonArray(NUMCOLS, NUMROWS);

                /* Variables to hold the number of columns and rows per amtrix */
                int pythonIntRowsOne = NUMROWS;
                int pythonIntColsOne = NUMCOLS;
                int pythonIntRowsTwo = NUMROWS;
                int pythonIntColsTwo = NUMCOLS;

                /* Matrix multiply int */
                int* pythonIntResult = makePythonArray(pythonIntColsTwo, pythonIntRowsOne);
                outFile << &pythonIntColsTwo << "\n";
                outFile << &pythonIntRowsOne << "\n";
                int temp;

                for (i = 0; i < pythonIntRowsOne; i++) {
                    outFile << &i << "\n";
                    outFile << &pythonIntRowsOne << "\n";
                    for (j = 0; j < pythonIntColsTwo; j++) {
                        outFile << &j << "\n";
                        outFile << &pythonIntColsTwo << "\n";
                        for (k = 0; k < pythonIntRowsTwo; k++) {
                            outFile << &k << "\n";
                            outFile << &pythonIntRowsTwo << "\n";
                            tempValue = getIntAt(pythonIntOne, pythonIntColsOne, k, i) * getIntAt(pythonIntTwo, pythonIntColsTwo, j, k);
                            temp = getIntAt(pythonIntOne, pythonIntColsOne, k, i);
                            outFile << &temp << "\n";
                            temp = getIntAt(pythonIntTwo, pythonIntColsTwo, j, k);
                            outFile << &temp << "\n";
                            outFile << &tempValue << "\n";
                            setIntAt(pythonIntResult, pythonIntColsTwo, j, i, tempValue);
                            outFile << &tempValue << "\n";
                            temp = getIntAt(pythonIntResult, pythonIntColsTwo, j, i);
                            outFile << &temp << "\n";
                            outFile << &k << "\n";
                        }
                        outFile << &j << "\n";
                    }
                    outFile << &i << "\n";
                }

                cout << &pythonIntResult;

                if (outFile.is_open()) {
                    outFile.close();    /* Close file once finished writing */
                }
            }
            else if (whatType == 1) { /*Double*/
                double* pythonDoubleOne = makePythonDoubleArray(NUMCOLS, NUMROWS);
                double* pythonDoubleTwo = makePythonDoubleArray(NUMCOLS, NUMROWS);

                int pythonDoubleRowsOne = NUMROWS;
                int pythonDoubleColsOne = NUMCOLS;
                int pythonDoubleRowsTwo = NUMROWS;
                int pythonDoubleColsTwo = NUMCOLS;

                /* Matrix multiply double */
                double* pythonDoubleResult = makePythonDoubleArray(pythonDoubleColsTwo, pythonDoubleRowsOne);
                double tempProduct;
                outFile << &pythonDoubleColsTwo << "\n";
                outFile << &pythonDoubleRowsOne << "\n";
                double tempd;

                for (i = 0; i < pythonDoubleRowsOne; i++) {
                    outFile << &i << "\n";
                    outFile << &pythonDoubleRowsOne << "\n";
                    for (j = 0; j < pythonDoubleColsTwo; j++) {
                        outFile << &j << "\n";
                        outFile << &pythonDoubleColsTwo << "\n";
                        for (k = 0; k < pythonDoubleRowsTwo; k++) {
                            outFile << &k << "\n";
                            outFile << &pythonDoubleRowsTwo << "\n";
                            tempProduct = getDoubleAt(pythonDoubleOne, pythonDoubleColsOne, k, i) * getDoubleAt(pythonDoubleTwo, pythonDoubleColsTwo, j, k);
                            tempd = getDoubleAt(pythonDoubleOne, pythonDoubleColsOne, k, i);
                            outFile << &tempd << "\n";
                            tempd = getDoubleAt(pythonDoubleTwo, pythonDoubleColsTwo, j, k);
                            outFile << &tempd << "\n";
                            outFile << &tempProduct << "\n";
                            setDoubleAt(pythonDoubleResult, pythonDoubleColsTwo, j, i, tempProduct);
                            outFile << &tempProduct << "\n";
                            tempd = getDoubleAt(pythonDoubleResult, pythonDoubleColsTwo, j, i);
                            outFile << &tempd << "\n";
                            outFile << &k << "\n";
                        }
                        outFile << &j << "\n";
                    }
                    outFile << &i << "\n";
                }

                cout << &pythonDoubleResult;

                if (outFile.is_open()) {
                    outFile.close();    /* Close file once finished writing */
                }

            }
            else if (whatType == 2) { /*Float*/

                float* pythonFloatOne = makePythonFloatArray(NUMCOLS, NUMROWS);
                float* pythonFloatTwo = makePythonFloatArray(NUMCOLS, NUMROWS);

                int pythonFloatRowsOne = NUMROWS;
                int pythonFloatColsOne = NUMCOLS;
                int pythonFloatRowsTwo = NUMROWS;
                int pythonFloatColsTwo = NUMCOLS;

                /* Matrix multiply float */
                float* pythonFloatResult = makePythonFloatArray(pythonFloatColsTwo, pythonFloatRowsOne);
                float floatValue;
                outFile << &pythonFloatColsTwo << "\n";
                outFile << &pythonFloatRowsOne << "\n";
                float tempf;

                for (i = 0; i < pythonFloatRowsOne; i++) {
                    outFile << &i << "\n";
                    outFile << &pythonFloatRowsOne << "\n";
                    for (j = 0; j < pythonFloatColsTwo; j++) {
                        outFile << &j << "\n";
                        outFile << &pythonFloatColsTwo << "\n";
                        for (k = 0; k < pythonFloatRowsTwo; k++) {
                            outFile << &k << "\n";
                            outFile << &pythonFloatRowsTwo << "\n";
                            floatValue = getFloatAt(pythonFloatOne, pythonFloatColsOne, k, i) * getFloatAt(pythonFloatTwo, pythonFloatColsTwo, j, k);
                            tempf = getFloatAt(pythonFloatOne, pythonFloatColsOne, k, i);
                            outFile << &tempf << "\n";
                            tempf = getFloatAt(pythonFloatTwo, pythonFloatColsTwo, j, k);
                            outFile << &tempf << "\n";
                            outFile << &floatValue << "\n";
                            setFloatAt(pythonFloatResult, pythonFloatColsTwo, j, i, floatValue);
                            outFile << &floatValue << "\n";
                            tempf = getFloatAt(pythonFloatResult, pythonFloatColsTwo, j, i);
                            outFile << &tempf << "\n";
                            outFile << &k << "\n";
                        }
                        outFile << &j << "\n";
                    }
                    outFile << &i << "\n";
                }

                cout << &pythonFloatResult;

                if (outFile.is_open()) {
                    outFile.close();    /* Close file once finished writing */
                }
            }
        }

    }
    else {  /*Do not performa actions if the file isn't open*/
        cout << "No valid file to write is open" << "\n";
    }
    
   
    std::cout << "Goodbye!\n";
}
